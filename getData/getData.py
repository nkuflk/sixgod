#!/usr/bin/python
# -*- coding: utf-8 -*-
#########################################################################
# File Name: test.py
# Author: nkuflk 
# mail: nkuflk@gmail.com 
# Created Time: 2014-08-29 16:55:38
#########################################################################

from weibo import APIClient

import SinaAPI
import yaml
import time
import MySQLdb

dayDict = {"Mon":0,"Tue":1,"Wed":2,"Thu":3,"Fri":4,"Sat":5,"Sun":6}
monthDict = {"Jan":1,"Feb":2,"Mar":3,"Apr":4,"May":5,"Jun":6,"Jul":7,"Aug":8,"Sep":9,"Oct":10,"Nov":11,"Dec":12}

def get_friends(client, uid, app_key):
    users = client.friendships.friends.get(uid=uid, source=app_key)['users']
    return users

def get_timeline(client):
    timeline = client.statuses.home_timeline.get(count=100)['statuses']
    return timeline

def get_public_timeline(client):
    timeline = client.statuses.public_timeline.get(count=200)['statuses']
    return timeline

def get_app_info():
    fileHandle = open('api.yaml','r')
    appinfo = yaml.load(fileHandle)
    fileHandle.close()
    return str(appinfo['APP_KEY']), appinfo['APP_SECRET'], appinfo['REDIRECT_URL']

def get_account_info():
    fileHandle = open('account.yaml','r')
    accountinfo = yaml.load(fileHandle)
    fileHandle.close()
    return accountinfo

def get_database_info():
    fileHandle = open('database.yaml','r')
    databaseinfo = yaml.load(fileHandle)
    fileHandle.close()
    return databaseinfo['account'], str(databaseinfo['passwd'])

def change_time(createTime):
    temp = createTime.split(' ')
    hms = temp[3].split(':')
    day = 366 if temp[5]=='2012' or temp[5]=='2008' else 365
    tm = (int(temp[5]),monthDict[temp[1]],int(temp[2]),int(hms[0]),int(hms[1]),int(hms[2]),dayDict[temp[0]],day,0)
    return str(int(time.mktime(tm)))

def login():
    APP_KEY, APP_SECRET, REDIRECT_URL = get_app_info()
    client = APIClient(app_key=APP_KEY, app_secret=APP_SECRET, redirect_uri=REDIRECT_URL)
    CALLBACK_URL = client.get_authorize_url()
    accountinfo = get_account_info()
    username = accountinfo['id']
    password = accountinfo['passwd']
    API = SinaAPI.SinaAPI(CALLBACK_URL, APP_KEY, REDIRECT_URL, username, password)
    code = API.get_code_Security()
    requests = client.request_access_token(code)  
    access_token = requests.access_token
    expires_in = requests.expires_in  
    uid = requests.uid
    client.set_access_token(access_token, expires_in)  
    return client

def writedb(timeline):
    account, passwd = get_database_info()
    db = MySQLdb.connect('10.101.84.106',account,passwd,'hack_weibo',charset='utf8')
    cursor = db.cursor()
    for weibo in timeline:
        seconds = change_time(weibo['created_at'])
        para = (str(weibo['id']),weibo['text'],seconds)
        sql = "insert into tb_raw_msg(msg_id,content,time) values(%s,%s,%s)"
        cursor.execute(sql,para)
        db.commit()
    db.close()
 
if __name__ == "__main__":
    client = login()
    timeline = get_timeline(client)
    writedb(timeline)
    timeline = get_public_timeline(client)
    writedb(timeline)
